using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataMaster : MonoBehaviour
{
    public static DataMaster instance;
    //public AudioClip[] audioList;
    public Collider[] spiritObjects;
    public Collider[] terrenalObjects;

    public Collider[] everyObjects;

    public AudioClip[] audioClips;
    public AudioClip[] audioClips2;
    public int index;
    public int objectCount;
    public AudioSource[] radio;

    public Color[] Colorlist;

    public GameObject[] m_Radios;

    public bool m_IspuzzleComplete = false;

    public GameObject m_AlertMessage;
    public GameObject m_NextInstruction;

    // Start is called before the first frame update

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }
    void Start()
    {
        objectCount = 0;
        index = 0;

        radio[0].clip = audioClips[index];
        radio[1].clip = audioClips2[index];

        foreach (Collider c in everyObjects)
        {
            c.enabled = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        //DeactivateColliders();
    }

    public void PickUpObject()
    {
        objectCount++;
        if (objectCount == 1)
        {
            index++;
            Debug.Log("Instrucciones");
            ActiveNextRadioInstruction();
            objectCount = 0;
        }
    }

    void DeactivateColliders()
    {
        foreach(Collider c in spiritObjects)
        {
            c.enabled = false;
        }
        spiritObjects[index].enabled = true;

        foreach (Collider c in terrenalObjects)
        {
            c.enabled = false;
        }
        terrenalObjects[index].enabled = true;

    }

    public void DeactivateCollidersEveryObject()
    {
        foreach (Collider c in everyObjects)
        {
            c.enabled = false;
        }
        everyObjects[index].enabled = true;        
    }


    public void ActiveNextRadioInstruction()
    {
        for(int i = 0; i < m_Radios.Length; i++)
        {
            m_Radios[i].GetComponent<MeshRenderer>().material.SetColor("_Color", Colorlist[index]);
        }

        if(index < audioClips.Length)
        {
            radio[0].clip = audioClips[index];
            radio[1].clip = audioClips2[index];
            ShowInstruction();
        }
        else
        {
            m_IspuzzleComplete = true;
            radio[0].gameObject.SetActive(false);
            radio[1].gameObject.SetActive(false);
        }
    }

    public void ShowAlert()
    {
        m_AlertMessage.SetActive(true);
        Invoke("DeactiveAlert", 3.0f);
    }

    public void DeactiveAlert()
    {
        m_AlertMessage.SetActive(false);
    }

    public void ShowInstruction()
    {
        m_NextInstruction.SetActive(true);
        Invoke("DeactiveInstruction", 3.0f);
    }

    public void DeactiveInstruction()
    {
        m_NextInstruction.SetActive(false);
    }
}
