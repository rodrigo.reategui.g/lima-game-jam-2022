using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Instructions : MonoBehaviour
{
    public GameObject PlayArea;
    public GameObject currentCanvas;
    public AudioSource m_AudioSource;

    // Start is called before the first frame update
    void Start()
    {
        m_AudioSource.Play();
    }

    // Update is called once per frame
    void Update()
    {
        if(m_AudioSource.isPlaying)
        {
            return;
        }
        else
        {
            PlayArea.SetActive(true);
            currentCanvas.SetActive(false);
        }
    }
}
