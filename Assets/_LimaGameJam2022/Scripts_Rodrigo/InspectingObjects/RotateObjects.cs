using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateObjects : MonoBehaviour
{
    float rotationSpeed = 30;
    public GameObject puzzle;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void RotateXPos()
    {
        puzzle.transform.Rotate(Vector3.right * rotationSpeed);
        
    }

    public void RotateXNeg()
    {
        puzzle.transform.Rotate(-Vector3.right * rotationSpeed);
    }

    public void RotateYPos()
    {
        puzzle.transform.Rotate(Vector3.up * rotationSpeed);

    }

    public void RotateYNeg()
    {
        puzzle.transform.Rotate(-Vector3.up * rotationSpeed);
    }
    public void RotateZPos()
    {
        puzzle.transform.Rotate(Vector3.forward * rotationSpeed);

    }

    public void RotateZNeg()
    {
        puzzle.transform.Rotate(-Vector3.forward * rotationSpeed);
    }
}
