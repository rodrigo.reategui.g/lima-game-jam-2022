using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    private AudioSource m_AudioSource;
    public AudioClip[] m_ListOfClips;
    public int m_Index = 0;

    public Camera m_Camera;
    private Rect m_CameraRect;

    // Start is called before the first frame update
    void Start()
    {
        m_AudioSource = GetComponent<AudioSource>();
        m_CameraRect = m_Camera.rect;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(0) && m_Camera.rect.width > 0.25f)
        {
            Debug.Log("Click00  " + name);

            RaycastHit hit;
            Ray ray = m_Camera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                Debug.Log("Click01 " + hit.collider.gameObject.name + hit.collider.gameObject.tag);

                if (hit.collider.gameObject.tag == "Respawn")
                {
                    Debug.Log("Click");
                    PlayAudio(m_Index);
                }
            }
        }
       
    }

    private void OnMouseDown()
    {
        Debug.Log("Click");
        PlayAudio(m_Index);
    }

    public void PlayAudio(int index)
    {
        //m_AudioSource.clip = m_ListOfClips[index];
        if(!m_AudioSource.isPlaying)
        {
            m_AudioSource.Play();
            DataMaster.instance.DeactivateCollidersEveryObject();
        }
        
    }
}
