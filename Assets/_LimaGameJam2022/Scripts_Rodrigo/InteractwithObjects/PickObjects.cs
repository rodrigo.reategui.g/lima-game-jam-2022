using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickObjects : MonoBehaviour
{
    [SerializeField]
    private float rangeDistance;
    [SerializeField]
    private LayerMask objectMask;
    private bool inRange;
    [SerializeField]
    private GameObject inventory;

    public Camera m_Camera;


    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && m_Camera.rect.width > 0.25f)
        {
            var ray = m_Camera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            inRange = Physics.Raycast(ray, rangeDistance, objectMask);
            if (Physics.Raycast(ray, out hit) && inRange)
            {
                //int imageforUI = hit.transform.GetComponent<HighlightObjects>().imageNumber;
                //inventory.GetComponent<InventoryManagement>().AddToInventory(imageforUI);
                DataMaster.instance.PickUpObject();
                hit.transform.gameObject.SetActive(false);
            }

        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawSphere(transform.position, rangeDistance);
    }
}
