using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleCompleted : MonoBehaviour
{
    public GameObject m_FinalPuzzle;

    private void Update()
    {
        Debug.Log(transform.localEulerAngles.x + "  " + transform.localEulerAngles.y + "  " + transform.localEulerAngles.z);
        if ((int)transform.localEulerAngles.y == 90 && (int)transform.localEulerAngles.x == 0 && (int)transform.localEulerAngles.z == 0)
        {
            Debug.Log("Puzzle Solved");
            m_FinalPuzzle.SetActive(true);
        }
    }
}
