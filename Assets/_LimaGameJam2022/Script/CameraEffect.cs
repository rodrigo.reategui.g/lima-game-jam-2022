using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CameraEffect : MonoBehaviour
{
    public Camera m_LeftCamera;
    public Camera m_RightCamera;

    private float m_ViewportRectMin = 0.0f;
    private float m_ViewportRectMax = 1.0f;
    private float m_ViewportWMin = 0.01f;
    private float m_ViewportWMax = 1.0f;

    static float t = 0.0f;
    static float t2 = 0.0f;
    public float m_EffectSpeed = 5.0f;

    public bool m_LeftCamComplete = false;
    public bool m_RightCamComplete = false;

    public RectTransform m_Image;
    public RectTransform m_LeftPos;
    public RectTransform m_RightPos;

    public AudioSource m_RealWorldAudioSource;
    public AudioSource m_SpiritWorldAudioSource;

    private float m_Width;

    public bool autochange = false;

    // Start is called before the first frame update
    void Start()
    {
        //m_Width = Screen.width / 4 - m_Image.rect.width / 2;
        m_Image.localPosition = m_RightPos.localPosition;
        //Debug.Log(m_Width + "  "  + m_Image.position);

        if (SceneManager.GetActiveScene().name == "MainMenu")
        {
            InvokeRepeating("AutoChangeCamera", 3.0f, 3.0f);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (SceneManager.GetActiveScene().name != "MainMenu")
        {
            if (Input.GetKeyDown(KeyCode.Q))
            {
                m_LeftCamComplete = true;
                m_RightCamComplete = false;
            }

            if (Input.GetKeyDown(KeyCode.E))
            {
                m_LeftCamComplete = false;
                m_RightCamComplete = true;
            }
        }



        if (m_LeftCamComplete)
        {
            ActiveLeftCamera();
            MoveEffectRight();
        }

        if (m_RightCamComplete)
        {
            ActiveRightCamera();
            MoveEffectLeft();
        }

        AudioControl(m_LeftCamera.rect.width, m_RightCamera.rect.width);

        
    }

    public void AutoChangeCamera()
    {
        if(autochange)
        {
            m_LeftCamComplete = true;
            m_RightCamComplete = false;
            autochange = !autochange;
        }
        else
        {
            m_LeftCamComplete = false;
            m_RightCamComplete = true;
            autochange = !autochange;
        }
    }

    public void ActiveLeftCamera()
    {
        m_LeftCamera.rect = new Rect(0.0f, 
                                     0.0f,
                                     Mathf.Lerp(m_LeftCamera.rect.width, m_ViewportWMax, t), 
                                     1.0f);

        m_RightCamera.rect = new Rect(Mathf.Lerp(m_RightCamera.rect.x, m_ViewportRectMax, t),
                                     0.0f,
                                     Mathf.Lerp(m_RightCamera.rect.width, m_ViewportWMin, t),
                                     1.0f);
        //MoveEffectRight();

        // .. and increase the t interpolater
        //t += 0.0005f * Time.deltaTime;
        t = m_EffectSpeed * Time.deltaTime;

    }

    public void ActiveRightCamera()
    {
        m_LeftCamera.rect = new Rect(0.0f,
                                     0.0f,
                                     Mathf.Lerp(m_LeftCamera.rect.width, m_ViewportWMin, t),
                                     1.0f);

        m_RightCamera.rect = new Rect(Mathf.Lerp(m_RightCamera.rect.x, m_ViewportRectMin, t),
                                     0.0f,
                                     Mathf.Lerp(m_RightCamera.rect.width, m_ViewportWMax, t),
                                     1.0f);

        //t += 0.0005f * Time.deltaTime;
        t = m_EffectSpeed * Time.deltaTime;
    }

    public void MoveEffectLeft()
    {
        m_Image.localPosition = new Vector3(Mathf.Lerp(m_Image.localPosition.x, m_LeftPos.localPosition.x, t2), 
            m_Image.localPosition.y, 
            m_Image.localPosition.z);
        //t2 += 0.0005f * Time.deltaTime;
        t2 = m_EffectSpeed * Time.deltaTime;
    }

    public void MoveEffectRight()
    {
        m_Image.localPosition = new Vector3(Mathf.Lerp(m_Image.localPosition.x, m_RightPos.localPosition.x, t2), 
            m_Image.localPosition.y, 
            m_Image.localPosition.z);
        //t2 += 0.0005f * Time.deltaTime;
        t2 = m_EffectSpeed * Time.deltaTime;
    }

    public void AudioControl(float realVolume, float spiritVolume)
    {
        m_RealWorldAudioSource.volume = realVolume / 2.0f;
        m_SpiritWorldAudioSource.volume = spiritVolume /2.0f;
    }

    public void NextLevel()
    {
        SceneManager.LoadScene(1);
    }
}
