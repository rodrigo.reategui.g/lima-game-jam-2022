using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LeaveScene : MonoBehaviour
{
    [SerializeField]
    private GameObject playArea;
    [SerializeField]
    private GameObject puzzleWorld;
    public void GoBack()
    {
        puzzleWorld.SetActive(false);
        playArea.SetActive(true);
    }
}
