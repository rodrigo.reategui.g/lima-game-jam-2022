using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private CharacterController m_CharacterController;
    public float m_Velocity = 12;

    private float X;
    private float Z;

    // Start is called before the first frame update
    void Start()
    {
        m_CharacterController = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {        
        X = Input.GetAxis("Horizontal");
        Z = Input.GetAxis("Vertical");

        Vector3 move = new Vector3(transform.right.x, 0, transform.right.z) * X +
                       new Vector3(transform.forward.x, 0, transform.forward.z) * Z;
        m_CharacterController.Move(move * m_Velocity * Time.deltaTime);
    }
}
