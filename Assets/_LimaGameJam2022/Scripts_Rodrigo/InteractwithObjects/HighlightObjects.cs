using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighlightObjects : MonoBehaviour
{
    [SerializeField]
    private float rangeDistance;
    [SerializeField]
    private LayerMask playerMask;
    private bool inRange;
    [SerializeField]
    private Renderer renderer;
    private bool hasBeenSelected;

    private void Start()
    {
        var renderer = GetComponent<Renderer>();

    }
    private void OnMouseOver()
    {
        inRange = Physics.CheckSphere(transform.position, rangeDistance, playerMask);
        if (inRange)
        {
            renderer.material.SetColor("_Color", Color.red);
            hasBeenSelected = true;
        }
        
    }

    private void OnMouseExit()
    {
        if (hasBeenSelected)
        {
            renderer.material.SetColor("_Color", Color.cyan);
            hasBeenSelected = false;
        }

    }
}
