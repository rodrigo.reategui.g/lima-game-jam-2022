using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryManagement : MonoBehaviour
{
    [SerializeField]
    private GameObject[] inventoryList;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AddToInventory(int a)
    {
        if (a > -1)
        {
            inventoryList[a].SetActive(true);
        }
    }

}
