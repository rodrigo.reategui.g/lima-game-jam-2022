using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActivateObjects : MonoBehaviour
{
    [SerializeField]
    private float rangeDistance;
    [SerializeField]
    private LayerMask playerMask;
    private bool inRange;
    [SerializeField]
    private GameObject playArea;
    [SerializeField]
    private GameObject puzzleWorld;



    private void OnMouseDown()
    {
        if(DataMaster.instance.m_IspuzzleComplete)
        {
            inRange = Physics.CheckSphere(transform.position, rangeDistance, playerMask);
            if (inRange)
            {
                puzzleWorld.SetActive(true);
                playArea.SetActive(false);

            }
        }
        else
        {
            DataMaster.instance.ShowAlert();
        }
    }


    private void OnDrawGizmos()
    {
        Gizmos.DrawSphere(transform.position, rangeDistance);
    }

    
}
