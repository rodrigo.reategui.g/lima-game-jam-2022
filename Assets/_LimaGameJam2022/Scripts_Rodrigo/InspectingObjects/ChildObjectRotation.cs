using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChildObjectRotation : MonoBehaviour
{
    [SerializeField]
    private float rotationSpeed;
    [SerializeField]
    private Camera cam;


    private void OnMouseDrag()
    {
        float rotX = Input.GetAxis("Mouse X") * rotationSpeed;
        float rotY = Input.GetAxis("Mouse Y") * rotationSpeed;

        Vector3 right = Vector3.Cross(cam.transform.up, transform.parent.position - cam.transform.position);
        Vector3 up = Vector3.Cross(transform.parent.position - cam.transform.position, right);
        transform.parent.rotation = Quaternion.AngleAxis(-rotX, up) * transform.parent.rotation;
        transform.parent.rotation = Quaternion.AngleAxis(rotY, right) * transform.parent.rotation;
    }
}
